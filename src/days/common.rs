use std::cmp::Ordering;
use std::fs::File;
use std::io::BufReader;

#[derive(Debug)]
pub struct Elf {
    pub no: usize,
    pub calories: usize
}

impl Ord for Elf {
    fn cmp(&self, other: &Self) -> Ordering {
        self.calories.cmp(&other.calories)
    }
}

impl PartialOrd for Elf {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl Eq for Elf {}

impl PartialEq for Elf {
    fn eq(&self, other: &Self) -> bool {
        self.calories == other.calories
    }
}

impl Elf {
    pub fn add_calories(&mut self, calories: usize){
        self.calories += calories;
    }
    pub fn new(no: usize) -> Elf {
        Elf { no: no, calories: 0 }
    }
}

pub fn open_file(name: &str) -> BufReader<File>{
    let file = File::open("resources/".to_owned() + name ).unwrap();
     return BufReader::new(file);
}