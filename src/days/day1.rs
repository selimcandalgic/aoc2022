pub use crate::days::common::Elf;
use crate::days::common::open_file;

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn find_calories(mut reader: BufReader<File>) -> Vec<Elf> {
    let mut line = String::new();
    let mut elf_counter = 0 as usize;
    let mut elf: Elf = Elf::new(elf_counter);
    let mut elves = Vec::new();
    
    while reader.read_line(&mut line).unwrap() > 0 {
        if line.chars().nth(0) == Some('\n'){
            elf_counter += 1;
            elves.push(elf);
            elf = Elf::new(elf_counter);
        }else {
            let calories = line.trim().parse::<usize>().unwrap();
            elf.add_calories(calories);
        }
        line.clear();
    }
    return elves;
}

pub fn solve(){
    let buf_reader = open_file("day1.txt");
    let mut elves = find_calories(buf_reader);
    elves.sort();
    let len = elves.len();
    let mut top3 = 0 as usize;
    for i in (len - 3)..(len) {
        let elf = &elves[i];
        println!("{:?}", elf);
        top3 += elf.calories;
    }
    println!("top3 total: {}", top3);
}