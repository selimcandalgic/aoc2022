use crate::days::common::open_file;
use std::io::BufRead;

struct Move {
    points: usize,
    mapping: char,
}
impl Move {
    fn new(points: usize, mapping: char) -> Move {
        Move {
            points: points,
            mapping: mapping,
        }
    }
}

pub fn solve() {
    let rock = Move {
        points: 1,
        mapping: 'A',
    };
    let paper = Move {
        points: 2,
        mapping: 'B',
    };
    let scissors = Move {
        points: 3,
        mapping: 'C',
    };
    let moves = [&rock, &paper, &scissors];
    let secret_moves = [&Move::new(1, 'X'), &Move::new(2, 'Y'), &Move::new(3, 'Z')];

    let rules = [(1, 3), (2, 1), (3, 2)];

    let mut buf_reader = open_file("day2.txt");
    let mut line = String::new();
    let mut result = 0 as usize;
    let mut second_result = 0 as usize;

    while buf_reader.read_line(&mut line).unwrap() > 0 {
        let mut chars = line.trim().split(' ');
        let opp = chars.next().unwrap().chars().nth(0).unwrap();
        let opp = get_move(moves, opp);
        let you = chars.next().unwrap().chars().nth(0).unwrap();
        let you = get_move(secret_moves, you);
        result += outcome(rules, opp, you);
        for (w, l) in rules {
            match you.mapping {
                'X' => {
                    // lose it
                    if opp.points == w {
                        second_result += l;
                    }
                }
                'Y' => {
                    // take a draw
                    if opp.points == w {
                        second_result += 3 + opp.points;
                    }
                }
                'Z' => {
                    // win this
                    if opp.points == l {
                        second_result += 6 + w;
                    }
                }
                _ => {}
            }
        }
        line.clear();
    }
    println!("first part: {}, second part: {}",result,second_result);
}

fn get_move(moves: [&Move; 3], sym: char) -> &Move {
    for m in moves {
        if m.mapping == sym {
            return m;
        }
    }
    // we should't panic
    panic!("");
}

fn outcome(rules: [(usize, usize); 3], opponent: &Move, you: &Move) -> usize {
    if you.points == opponent.points {
        return 3 + you.points;
    } else {
        for (w, l) in rules.iter() {
            if *w == you.points && *l == opponent.points {
                return 6 + you.points;
            }
        }
        return you.points;
    }
}
