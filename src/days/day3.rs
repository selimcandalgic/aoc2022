use crate::days::common::open_file;
use std::io::BufRead;

pub fn solve() {
    let mut buf_reader = open_file("day3.txt");
    let mut line = String::new();
    let mut result = 0 as usize;
    let mut badge_result = 0 as usize;
    let mut i = 0 as usize;
    let mut group = ["".to_string(), "".to_string(), "".to_string()];

    while buf_reader.read_line(&mut line).unwrap() > 0 {
        let contents = String::from(line.trim());
        let len = contents.len() / 2;
        let first = &contents[..len];
        let second = &contents[len..];
        for i in 0..len {
            let ch = first.chars().nth(i).unwrap();
            match second.find(ch) {
                Some(_) => {
                    result += get_priority(ch);
                    break;
                }
                None => {}
            }
        }
        let idx = i % 3;
        group[idx] = contents;
        if idx == 2 {
            badge_result += process_group(&group);
        }
        i += 1;
        line.clear();
    }
    println!("sum of priorities: {}", result);
    println!("sum of badge priorities: {}", badge_result);
}

fn process_group(group: &[String; 3]) -> usize {
    for c1 in group[0].chars() {
        for c2 in group[1].chars() {
            for c3 in group[2].chars() {
                if c1 == c2 && c1 == c3 {
                    return get_priority(c1);
                }
            }
        }
    }
    panic!("should not happen");
}

fn get_priority(ch: char) -> usize {
    if ch.is_uppercase() {
        return ch as usize - 38;
    } else {
        return ch as usize - 96;
    }
}
