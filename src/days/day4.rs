use crate::days::common::open_file;
use std::io::BufRead;

pub fn solve() {
    let mut buf_reader = open_file("day4.txt");
    let mut line = String::new();
    let mut counter = 0 as usize;
    let mut overlap_counter = 0 as usize;

    while buf_reader.read_line(&mut line).unwrap() > 0 {
        let mut pairs = line.trim().split(',');
        let mut asgn1 = pairs.next().unwrap().split('-');
        let start1 = asgn1.next().unwrap().parse::<usize>().unwrap();
        let end1 = asgn1.next().unwrap().parse::<usize>().unwrap();
        let mut asgn2 = pairs.next().unwrap().split('-');
        let start2 = asgn2.next().unwrap().parse::<usize>().unwrap();
        let end2 = asgn2.next().unwrap().parse::<usize>().unwrap();
        if contains((start1, end1), (start2, end2)) {
            counter += 1;
            overlap_counter += 1;
        } else if overlaps((start1, end1), (start2, end2)) {
            overlap_counter += 1;
        }
        line.clear();
    }
    println!("fully contains: {}", counter);
    println!("overlapping: {}", overlap_counter);
}

fn contains(first: (usize, usize), second: (usize, usize)) -> bool {
    let (first_start, first_end) = first;
    let (second_start, second_end) = second;
    (first_start >= second_start && first_end <= second_end)
        || (second_start >= first_start && second_end <= first_end)
}

fn overlaps(first: (usize, usize), second: (usize, usize)) -> bool {
    let (first_start, first_end) = first;
    let (second_start, second_end) = second;
    !(first_end < second_start || first_start > second_end)
}
