use crate::days::common::open_file;
use std::io::BufRead;

pub fn solve(){
    solve_internal(false);
    solve_internal(true);
}

fn solve_internal(new_model: bool) {
    let mut buf_reader = open_file("day5.txt");
    let mut line = String::new();
    let mut stacks: Vec<Vec<char>> = Vec::new();
    let mut stack_count = 0;
    let mut read_stacks = true;
    while buf_reader.read_line(&mut line).unwrap() > 0 {
        if read_stacks {
            if line.len() == 1 {
                read_stacks = false;
                for i in 0..stack_count {
                    stacks[i].pop();
                    stacks[i].reverse();
                }
                continue;
            }
            if stack_count == 0 {
                stack_count = (line.len() - 1) / 3 - 2;
                for _ in 0..stack_count {
                    let stack = Vec::new();
                    stacks.push(stack);
                }
            }
            for i in 0..stack_count {
                let loc = 1 + i * 4;
                let ch = line.chars().nth(loc).unwrap();
                if ch != ' ' {
                    stacks[i].push(ch);
                }
            }
        } else {
            let mut split = line.trim().split(' ');
            let m = split.nth(1).unwrap().parse::<usize>().unwrap();
            let f = split.nth(1).unwrap().parse::<usize>().unwrap();
            let t = split.nth(1).unwrap().parse::<usize>().unwrap();
            let len = stacks[t - 1].len();
            for _ in 0..m {
                let ch = stacks[f - 1].pop().unwrap();
                if new_model {
                    stacks[t - 1].insert(len, ch);
                } else {
                    stacks[t - 1].push(ch);
                }
            }
        }
        line.clear();
    }
    if new_model {
        print!("top of the stacks with new model: ");
    } else {
        print!("top of the stacks: ");
    }
    for i in 0..stack_count {
        print!("{}", stacks[i].pop().unwrap())
    }
    println!("");
}
