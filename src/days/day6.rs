use std::io::BufRead;

use crate::days::common::open_file;

pub fn solve() {
    solve_internal(4);
    solve_internal(14);
}

fn solve_internal(char_count: u8) {
    let mut buf_reader = open_file("day6.txt");
    let mut line = String::new();
    let mut signal = Vec::new();

    buf_reader.read_line(&mut line).unwrap();
    for i in  0..line.len() {
        let ch = line.chars().nth(i).unwrap();
        signal.push(ch);
        if signal.len() == char_count.into() {
            if check_signal(&signal) {
                println!("signal starts at {}", i+1);
                return;
            }
            signal = signal[1..].to_vec();
        }
    }
}

fn check_signal(signal: &Vec<char>) -> bool{
    for i in 0..signal.len() - 1 {
        let ch = signal[i];
        for y in i + 1..signal.len() {
            if ch == signal[y]{
                return false;
            }
        }
    }
    return true;
}