use crate::days::day2;
use crate::days::day1;
use crate::days::day3;
use crate::days::day4;
use crate::days::day5;
use crate::days::day6;
pub mod days;


fn main() {
    day1::solve();
    day2::solve();
    day3::solve();
    day4::solve();
    day5::solve();
    day6::solve();
}
 